# APPIUM WITH SELECTABLE CHROMEDRIVER
A docker image built upon the official [Docker Image for Appium Android](https://hub.docker.com/r/appium/appium/), adding a specific chromedriver version for better compatibility with webviews.

See [girade/appium-with-chromedriver](https://hub.docker.com/repository/docker/girade/appium-with-chromedriver) on Docker Hub.

## USAGE

* Consult the official [appium docker image](https://hub.docker.com/r/appium/appium/) first.
* TL;DR: `docker run --privileged -d --rm -p 4723:4723
    -v /dev/bus/usb:/dev/bus/usb
    girade/appium-with-chromedriver:<your-chromium-version-here>`
* Add `"chromedriverExecutable": "/root/chromedriver"` to your test's `desiredCapabilities` when using this image as your Appium Hub.

## BUILDING
```bash
docker build \
    -t appium-with-chromedriver \
    --build-arg CHROMEDRIVER_VERSION=<your_chromedriver_version> .
```
