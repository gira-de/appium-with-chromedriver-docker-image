FROM appium/appium:latest
WORKDIR /root
ARG CHROMEDRIVER_VERSION
RUN curl https://chromedriver.storage.googleapis.com/${CHROMEDRIVER_VERSION}/chromedriver_linux64.zip -o chromedriver.zip \
    && unzip chromedriver.zip \
    && rm chromedriver.zip
CMD /root/wireless_autoconnect.sh && /root/entry_point.sh
